package com.app.springboot.rest.dailyinfo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DailyinfoApplication {

    public static void main(String[] args) {
        SpringApplication.run(DailyinfoApplication.class, args);
    }
}

