package com.app.springboot.rest.dailyinfo.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name = "RequestCounter")
@Table(name = "request_counter")
public class RequestCounter {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Enumerated
    @Column(name = "counter_type", columnDefinition = "smallint")
    private CounterType counterType;

    @Column(name = "currency_code", length = 3)
    private String currencyCode;

    @Column(name = "call_count")
    private long callCount;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public CounterType getCounterType() {
        return counterType;
    }

    public void setCounterType(CounterType counterType) {
        this.counterType = counterType;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public long getCallCount() {
        return callCount;
    }

    public void setCallCount(long callCount) {
        this.callCount = callCount;
    }
}
