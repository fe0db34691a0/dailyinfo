package com.app.springboot.rest.dailyinfo.controller;

import com.app.springboot.rest.dailyinfo.service.GetCursOnDateXMLService;
import com.app.springboot.rest.dailyinfo.service.RequestCounterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Controller
public class ActionController {
    Logger logger = LogManager.getLogger(ActionController.class);

    @Autowired
    private GetCursOnDateXMLService serviceSOAP;

    @Autowired
    RequestCounterService requestCounterService;

    @PostMapping(value = "/action")
    @ResponseBody
    public ResponseEntity action(@RequestBody Currency currency) {
        logger.info("post '/action' is called with code = {}", currency.getCode());

        boolean isCurrensyExists = serviceSOAP.getCursOnDateByCurrencyCode(currency.getCode());
        boolean responseResult = requestCounterService.processSOAPResponse(isCurrensyExists, currency.getCode());

        logger.info("post '/action' returned code = {}", responseResult ? HttpStatus.OK : HttpStatus.NOT_FOUND);

        return new ResponseEntity(responseResult ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }
}