package com.app.springboot.rest.dailyinfo.controller.response;

public class CounterStatus {
    private final String counterName;
    private final String currencyCode;
    private final long counterValue;


    public CounterStatus(String counterName, String currencyCode, long counterValue) {
        this.counterName = counterName;
        this.currencyCode = currencyCode;
        this.counterValue = counterValue;
    }

    public long getCounterValue() {
        return counterValue;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public String getCounterName() {
        return counterName;
    }
}
