package com.app.springboot.rest.dailyinfo.repository;

import com.app.springboot.rest.dailyinfo.entity.CounterType;
import com.app.springboot.rest.dailyinfo.entity.RequestCounter;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface  RequestCounterRepositiry extends CrudRepository<RequestCounter, Long> {

    /*
    *   Method provides list of all existing counter objects
    * */
    List<RequestCounter> findAll();

    /*
    *   Method returns Counter by it's currency code
    * */
    RequestCounter findByCurrencyCode(String currencyCode);

    /*
    *   Method returns Counter by it's counter type(enum CounterType):
    *       ALL_REQUESTS
    *       ALL_SUCCESS_REQUESTS
    *       CURRENCY_SUCCESS_REQUESTS
    * */
    RequestCounter findByCounterType(CounterType counterType);
}
