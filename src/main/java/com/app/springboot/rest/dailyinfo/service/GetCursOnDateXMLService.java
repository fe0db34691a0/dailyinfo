package com.app.springboot.rest.dailyinfo.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.apache.xerces.dom.ElementNSImpl;
import org.apache.xerces.dom.TextImpl;
import org.springframework.stereotype.Service;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import ru.cbr.web.*;
import ru.cbr.web.GetCursOnDateXMLResponse.GetCursOnDateXMLResult;

import javax.xml.ws.WebServiceException;
import java.util.List;

@Service
public class GetCursOnDateXMLService {
    Logger logger = LogManager.getLogger(GetCursOnDateXMLService.class);

    public static final String VCODE = "Vcode";

    public boolean getCursOnDateByCurrencyCode(String currencyCode) {
        boolean operationResult = false;

        try {
            DailyInfo service = new DailyInfo();
            DailyInfoSoap dailyInfoSoap = service.getDailyInfoSoap();
            GetCursOnDateXMLResult xmlResult = dailyInfoSoap.getCursOnDateXML(dailyInfoSoap.getLatestDateTime());

            operationResult = isCurrencyExist(currencyCode, xmlResult);
        } catch (WebServiceException e) {
            logger.error("Cannot call SOAP service GetCursOnDateXML with currency code = {}", currencyCode, e);
        }

        return operationResult;
    }

    private boolean isCurrencyExist(String currencyCode, GetCursOnDateXMLResult xmlResult) {
        List<Object> list = xmlResult.getContent();
        ElementNSImpl item = (ElementNSImpl) list.get(0);
        NodeList childNodeList = item.getElementsByTagName(VCODE);

        for (int i = 0; i < childNodeList.getLength(); i++) {
            Node currencyChildNode = childNodeList.item(i);
            TextImpl firstChild = (TextImpl) currencyChildNode.getFirstChild();
            String chVal = firstChild.getData();

            if (chVal.equalsIgnoreCase(currencyCode)) {
                return true;
            }
        }

        return false;
    }
}
