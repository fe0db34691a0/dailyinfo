package com.app.springboot.rest.dailyinfo.controller;

import com.app.springboot.rest.dailyinfo.controller.response.CounterStatus;
import com.app.springboot.rest.dailyinfo.service.RequestCounterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

@Controller
public class StatisticController {
    Logger logger = LogManager.getLogger(StatisticController.class);

    @Autowired
    RequestCounterService service;

    @RequestMapping(value = "/stat", method = RequestMethod.GET)
    @ResponseBody
    public List<CounterStatus> stat() {
        logger.info("get '/stat' is called");
        return service.getAllCounters();
    }
}