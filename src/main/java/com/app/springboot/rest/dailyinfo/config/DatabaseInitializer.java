package com.app.springboot.rest.dailyinfo.config;

import com.app.springboot.rest.dailyinfo.entity.CounterType;
import com.app.springboot.rest.dailyinfo.entity.RequestCounter;
import com.app.springboot.rest.dailyinfo.repository.RequestCounterRepositiry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
public class DatabaseInitializer implements ApplicationRunner {
    private final int INIT_CALL_VALUE = 0;
    private RequestCounterRepositiry requestCounterRepositiry;

    @Autowired
    public DatabaseInitializer(RequestCounterRepositiry requestCounterRepositiry) {
        this.requestCounterRepositiry = requestCounterRepositiry;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        requestCounterRepositiry.save(createRequestCounterItem(CounterType.ALL_REQUESTS,null, INIT_CALL_VALUE));
        requestCounterRepositiry.save(createRequestCounterItem(CounterType.ALL_SUCCESS_REQUESTS,null, INIT_CALL_VALUE));
    }

    private RequestCounter createRequestCounterItem(CounterType counterType, String currencyCode, long callCount){
        RequestCounter requestCounterItem = new RequestCounter();
        requestCounterItem.setCounterType(counterType);
        requestCounterItem.setCurrencyCode(currencyCode);
        requestCounterItem.setCallCount(callCount);

        return requestCounterItem;
    }
}
