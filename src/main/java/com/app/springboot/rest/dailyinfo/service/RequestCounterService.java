package com.app.springboot.rest.dailyinfo.service;

import com.app.springboot.rest.dailyinfo.controller.response.CounterStatus;
import com.app.springboot.rest.dailyinfo.entity.CounterType;
import com.app.springboot.rest.dailyinfo.entity.RequestCounter;
import com.app.springboot.rest.dailyinfo.repository.RequestCounterRepositiry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class RequestCounterService {
    @Autowired
    RequestCounterRepositiry repository;

    @Transactional(propagation = Propagation.REQUIRES_NEW, isolation = Isolation.SERIALIZABLE)
    public boolean processSOAPResponse(boolean responseResult, String currencyCode) {
        incrementAllRequestsCounter();
        if (responseResult) {
            incrementAllSuccessRequestsCounter();
            createOrIncrementByCurrencyCode(currencyCode);
            return true;
        }
        return false;
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW, isolation = Isolation.SERIALIZABLE)
    public List<CounterStatus> getAllCounters() {
        List<RequestCounter> countersList = repository.findAll();
        List<CounterStatus> counterStatus = new ArrayList<>();

        for (RequestCounter item : countersList) {
            counterStatus.add(
                    new CounterStatus(
                            item.getCounterType().name(),
                            item.getCurrencyCode(),
                            item.getCallCount()
                    )
            );
        }

        return counterStatus;
    }

    private void incrementAllRequestsCounter() {
        incrementCounterByType(CounterType.ALL_REQUESTS);
    }

    private void incrementAllSuccessRequestsCounter() {
        incrementCounterByType(CounterType.ALL_SUCCESS_REQUESTS);
    }

    private void createOrIncrementByCurrencyCode(String currencyCode) {
        RequestCounter counter = repository.findByCurrencyCode(currencyCode);
        if (counter == null) {
            counter = createRequestCounterWithCurrencyCode(currencyCode);
        }
        incrementCallCount(counter);
        repository.save(counter);
    }

    private void incrementCounterByType(CounterType type) {
        RequestCounter counter = repository.findByCounterType(type);
        counter.setCallCount(counter.getCallCount() + 1);
        repository.save(counter);
    }

    private RequestCounter createRequestCounterWithCurrencyCode(String currencyCode) {
        RequestCounter requestCounterItem = new RequestCounter();
        requestCounterItem.setCounterType(CounterType.CURRENCY_SUCCESS_REQUESTS);
        requestCounterItem.setCurrencyCode(currencyCode);
        requestCounterItem.setCallCount(0);

        return requestCounterItem;
    }

    private RequestCounter incrementCallCount(RequestCounter requestCounter) {
        requestCounter.setCallCount(requestCounter.getCallCount() + 1);

        return requestCounter;
    }
}
