package com.app.springboot.rest.dailyinfo.entity;

public enum CounterType {
    ALL_REQUESTS(0),
    ALL_SUCCESS_REQUESTS(1),
    CURRENCY_SUCCESS_REQUESTS(2);

    private final int counterValue;

    private CounterType(int counterValue) {
        this.counterValue = counterValue;
    }

    public int getValue() {
        return counterValue;
    }
}
